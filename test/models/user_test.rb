ENV['RAILS_ENV'] ||= 'test'
require 'test_helper'

class UserTest < ActiveSupport::TestCase
  fixtures :all
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                      password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                        first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')  
  end
  
  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end 
  
  test "should follow and unfollow a user" do
    shiaoli = users(:shiaoli)
    shiaoli2 = users(:shiaoli2)
    assert_not shiaoli.following?(shiaoli2)
    shiaoli.follow( shiaoli2)
    assert shiaoli.following?(shiaoli2)
    assert shiaoli2.followers.include?(shiaoli)
    shiaoli.unfollow(shiaoli2)
    assert_not shiaoli.following?(shiaoli2)
  end
  
  test "feed should have the right posts" do
    shiaoli = users(:shiaoli)
    shiaoli2 = users(:shiaoli2)
    lana = users(:lana)
    # 关注的用户发布的微博
    lana.microposts.each do |post_following|
      assert shiaoli.feed.include?(post_following)
    end
    # 自己的微博
    shiaoli.microposts.each do |post_self|
      assert shiaoli.feed.include?(post_self)
    end
    # 未关注用户的微博
    shiaoli2.microposts.each do |post_unfollowed|
      assert_not shiaoli.feed.include?(post_unfollowed)
    end
  end
end
