#User.create!(name: "Example User",
#             email: "example@railstutorial.org",
#             password: "foobar",
#             password_confirmation: "foobar",
#             admin: true,
#             activated: true,
#             activated_at: Time.zone.now)
            
User.create!(name: "Shiao Li",
             email: "lishiao2018@gmail.com",
             password: "foobarbar",
             password_confirmation: "foobarbar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

52.times do |n|
  name = "#{n+1}test"
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do |n|
  content = #{n+1}test microposts"
  users.each { |user| user.microposts.create!(content: content) }
end

# Following relationships
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }